package com.simpledemo.mydemo;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class Display_Data extends AppCompatActivity {
    private Db_Helper db;
    EditText editid,editname,editage,editaddress,edittimestamp;
    Button submit,view,update,delete,clear;
    public  String id,name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db=new Db_Helper(this);
        setContentView(R.layout.activity_display__data);
        //edit text fields : get data from here
        editid=(EditText)findViewById(R.id.editid);
        editname=(EditText)findViewById(R.id.editname);
        editage=(EditText)findViewById(R.id.editage);
        editaddress=(EditText)findViewById(R.id.editaddress);
        edittimestamp=(EditText)findViewById(R.id.edittimestamp);
        //Button declarations
        submit=(Button)findViewById(R.id.submit);
        view=(Button)findViewById(R.id.view);
        update=(Button)findViewById(R.id.update);
        delete=(Button)findViewById(R.id.delete);
        clear=(Button)findViewById(R.id.clear);
        //initialiations
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
            String id=editid.getText().toString();
            String name=editname.getText().toString();
            String age=editage.getText().toString();
            String address=editaddress.getText().toString();
                db.insert_data(name,age,address);
                   // db.insert_data(id,name,age,address);
                Toast.makeText(getApplicationContext(),"Data Submitted",Toast.LENGTH_SHORT).show();
                db.close();}
                catch (SQLiteException e){
                    Toast.makeText(getApplicationContext(),"Data not submitted",Toast.LENGTH_SHORT).show();
                }
            }
        });

        //view buttononclick listenerr
       view=(Button)findViewById(R.id.view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                  //  id = editname.getText().toString();
                   name=editname.getText().toString();
                   // Cursor cursor = db.getData(id);
                    Cursor cursor = db.getData(name);
                    String age = null;
                   // String id=null;
                   // String name=null;
                    String address = null;
                    String timestamp=null;
                    //moving the cursor to first to start reading data
                    if (cursor.moveToFirst()) {
                        do {
                            //getting data from cursor
                            id = cursor.getString(cursor.getColumnIndex("id"));
                            //name = cursor.getString(cursor.getColumnIndex("name"));
                            age = cursor.getString(cursor.getColumnIndex("age"));
                            address = cursor.getString(cursor.getColumnIndex("address"));
                            timestamp=cursor.getString(cursor.getColumnIndex("timestamp"));
                            System.out.println(id + "-----" + name + "---" + age + "-----" + address+"-------"+timestamp);
                        }
                        while (cursor.moveToNext());

                    }
                    cursor.close();
                 //setting up edit textfields with new data
                    editage.setText(age);
                    editaddress.setText(address);
                    editid.setText(id);
                    edittimestamp.setText(timestamp);
                    db.close();
                }
                catch(SQLiteException e){
                   Toast.makeText(getApplicationContext(),"Enter Correct ID",Toast.LENGTH_SHORT).show();
                }
            } });

        //clear button onclick listener
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               editid.setText("");
                editname.setText("");
                editage.setText("");
                editaddress.setText("");
            }
        });
        //on update click listener
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             try {
                 String id = editid.getText().toString();
                 String name = editname.getText().toString();
                 String age = editage.getText().toString();
                 String address = editaddress.getText().toString();
                 db.update_data(id, name, age, address);
                 System.out.println("update-----" + name + "---" + age + "-----" + address);
                 Toast.makeText(getApplicationContext(),"Record Updated",Toast.LENGTH_SHORT).show();
                 db.close();
             }
             catch (SQLiteException e){
                 Toast.makeText(getApplicationContext(),"Not Updated",Toast.LENGTH_SHORT).show();
             }
            }
        });

        //on delete click  listener
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String id = editid.getText().toString();
                    db.delete(id);                 //deleting id
                    System.out.println("delete:" + id);
                    Toast.makeText(getApplicationContext(),"Record deleted",Toast.LENGTH_SHORT).show();
                    db.close();
                } catch (SQLiteException e) {
                    Toast.makeText(getApplicationContext(),"Delete didn't work",Toast.LENGTH_SHORT).show();
                }
            } });
    }

}
