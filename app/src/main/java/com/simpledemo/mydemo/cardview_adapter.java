package com.simpledemo.mydemo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

/**
 * Created by Priyanshi on 22-Feb-17.
 */

public class cardview_adapter extends RecyclerView.Adapter<cardview_adapter.ViewHolder> {
public List<card> cardlist;
    public cardview_adapter( List<card> card_list) {
        this.cardlist=card_list;
    }

    @Override
    public cardview_adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemview= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view,parent,false);
        return new ViewHolder(itemview);
    }
//on bind viw holder getting nanme and email form position
    @Override
    public void onBindViewHolder(cardview_adapter.ViewHolder holder, int position) {
    card card_list=cardlist.get(position);
        holder.name.setText(card_list.getname());
        holder.email.setText(card_list.getemail());
    }

    @Override
    public int getItemCount() {
        return cardlist.size();
    }

    //VIewHolder when  multiple itmes are in a card
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name,email;
        public ViewHolder(View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.namelayout);
            email=(TextView)itemView.findViewById(R.id.emaillayout);
        }
    }
}
