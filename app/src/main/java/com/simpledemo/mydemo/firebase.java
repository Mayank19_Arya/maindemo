package com.simpledemo.mydemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.firebase.client.*;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.*;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.ValueEventListener;


public class firebase extends AppCompatActivity {
    private DatabaseReference databaseReference;
    private FirebaseDatabase firebasereference;
    EditText name,address;
    Button save,read;
    User user;
    String TAG="killer";
    public static String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_firebase);
        //getting data from user
        name=(EditText)findViewById(R.id.editname);
        address=(EditText)findViewById(R.id.editaddress);

        //
        firebasereference=FirebaseDatabase.getInstance();
        databaseReference=firebasereference.getReference("user");
       // firebasereference.getReference("MyDemo").setValue("Realtimedatabase");
        userId = databaseReference.push().getKey();
        save=(Button)findViewById(R.id.savebutton);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userId = databaseReference.push().getKey();
                String oname=name.getText().toString();
                String oaddress=address.getText().toString();
                User user=new User(oname,oaddress);
                databaseReference.child(userId).setValue(user);
            }
        });
        read=(Button)findViewById(R.id.readbutton);
        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference.child(userId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user=dataSnapshot.getValue(User.class);

                        Log.d(TAG, "Name : " + user.getname() + ", Address: " + user.getaddress());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "Failed to read value.");
                    }
                });
            }
        });
    }
}
    /*private FirebaseAnalytics firebaseAnalytics;
    EditText name,address;
    FirebaseDatabase firebaseDatabase;
    Button save;
    Firebase ref;
    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_firebase);
        firebaseAnalytics= FirebaseAnalytics.getInstance(this);
        //
        mDatabase=FirebaseDatabase.getInstance().getReference();
        DatabaseReference dref=mDatabase.getRef("kill");
        //databaseReference=FirebaseDatabase.getInstance().getReference();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        firebaseDatabase=FirebaseDatabase.getInstance();
        //


        name=(EditText)findViewById(R.id.editname);
        address=(EditText)findViewById(R.id.editaddress);
        save=(Button)findViewById(R.id.savebutton);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //ref=new Firebase(com.simpledemo.mydemo.Config.FIREBASE_URL);
                ref=new Firebase("https://mydemo-1b9d7.firebaseio.com/");
                String oname=name.getText().toString();
                String oaddress=address.getText().toString();
                Bundle bundle=new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME,oname);
                bundle.putString(FirebaseAnalytics.Param.ITEM_LOCATION_ID,oaddress);
                ref.child("data").setValue(bundle);
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT,bundle);
            }
        });
    }*/

