package com.simpledemo.mydemo;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class Splash_Screen extends AppCompatActivity {
CustomPagerAdapter customPagerAdapter;
ViewPager viewPager;
    private  int position;
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);
        TabLayout tabLayout = (TabLayout)findViewById(R.id.tab_layout);
        customPagerAdapter =new CustomPagerAdapter(getSupportFragmentManager(), getApplicationContext());
        viewPager=(ViewPager)findViewById(R.id.scrollview);
        viewPager.setAdapter(customPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }
    //automatic fragment changer in spash screen
    Handler handler =new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            if( position >= 4){
                Intent intent = new Intent(Splash_Screen.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
            else{
                position = position+1;
            }
            viewPager.setCurrentItem(position, true);
            handler.postDelayed(runnable, 2000);
        }
    };
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        viewPager.removeCallbacks(runnable);
        this.finish();
    }
    public  void onResume(){
        super.onResume();
        viewPager.postDelayed(runnable,2000);
    }
  public void onStop(){
      super.onStop();
      finish();
  }
    public void i(){
        Intent intent = new Intent(Splash_Screen.this,MainActivity.class);
        startActivity(intent);
    }
}