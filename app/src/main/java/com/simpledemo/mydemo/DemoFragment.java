package com.simpledemo.mydemo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class DemoFragment extends Fragment {
TextView textView;
//setting image to be displayed
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview=inflater.inflate(R.layout.fragment_demo, container, false);
        textView=(TextView)rootview.findViewById(R.id.page);
        rootview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                closeFragment();
                return false;
            }
        });
        return rootview;
}
    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(DemoFragment.this).commit();
    }

}
