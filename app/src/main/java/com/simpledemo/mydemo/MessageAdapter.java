package com.simpledemo.mydemo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.ColorRes;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends ArrayAdapter<ChatMessage> {
    /*   private Activity activity2;
       private List<ChatMessage> messages;
       private TextView msg;
       public LinearLayout linearLayout;

       public MessageAdapter(Activity activity, int context, List<ChatMessage> message) {
           super(activity, context, message);
           this.activity2 = activity;
           this.messages = message;
       }

     /*  public View getView(int position, View convertview, ViewGroup parent) {
           ViewHolder holder;
           LayoutInflater layoutInflater = (LayoutInflater) activity2.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
           int layoutresource=0;
               ChatMessage chatMessage = getItem(position);
               int viewtype = getItemViewType(position);
               if (chatMessage.getMine()) {
                   layoutresource = R.layout.activity_chat_right;
               } else {
                   layoutresource = R.layout.activity_chat_left;
               }
               if (convertview == null) {

                   convertview = layoutInflater.inflate(layoutresource, parent, false);
                   holder = new ViewHolder(convertview);
                   convertview.setTag(holder);
               } else {

                   holder = (ViewHolder) convertview.getTag();
                  //convertview.setTag(holder);
               }
               holder.msg.setText(chatMessage.getContent());
        return convertview;
       }
       public int getViewTypeContent(){
           return 2;
       }
       public int getItemViewType(int position){
               return position % 2;
       }

   }*/
    private Activity activity2;
    private List<ChatMessage> messages;
    private TextView chatText;
    private LinearLayout singleMessageContainer;

    public MessageAdapter(Activity activity, int context, List<ChatMessage> message) {
        super(activity, context, message);
        this.activity2 = activity;
        this.messages = message;
    }
    public int getCount() {
        return this.messages.size();
    }

    //setting view of each item according to position
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_message, parent, false);
        }
        singleMessageContainer = (LinearLayout) row.findViewById(R.id.singleMessageContainer);
        ChatMessage chatMessageObj = getItem(position);
        chatText = (TextView) row.findViewById(R.id.singleMessage);
        chatText.setText(chatMessageObj.getContent());
        chatText.setTextColor(R.color.white);
        chatText.setPadding(45,5,45,5);

        //setting left or right gravity and backgorund resource
        chatText.setBackgroundResource(chatMessageObj.getMine() ? R.drawable.chat_bubble_right : R.drawable.chat_bubble_left);
        singleMessageContainer.setGravity(chatMessageObj.getMine() ? Gravity.RIGHT : Gravity.LEFT);
        return row;
    }


}