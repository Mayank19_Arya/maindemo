package com.simpledemo.mydemo;


import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import    android.support.v4.app.FragmentManager;
import  android.support.v4.app.FragmentTransaction;


public class Recycler_information extends AppCompatActivity {
    Custom_Main customPagerAdapter2;
    ViewPager viewPager2;
    int position;
    public List<Get_Data>  name_list=new ArrayList<>();
    public Layout_Adapter layout_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_information);
       // TabLayout tabLayout = (TabLayout)findViewById(R.id.tab_layout2);
        customPagerAdapter2 =new Custom_Main(getSupportFragmentManager(),Recycler_information.this);
        viewPager2=(ViewPager)findViewById(R.id.scrollview2);
        viewPager2.setAdapter(customPagerAdapter2);

        final RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        layout_adapter=new Layout_Adapter(name_list); //initialising adapter with list

        //adding the data
        Get_Data newdata=new Get_Data("Mahatma Gandhi","Born and raised in aHindu merchant caste family in coastal Gujarat, western India, and trained in law at the Inner Temple, London, Gandhi first employed nonviolent civil disobedience as an expatriate lawyer in South Africa, in the resident Indian community's struggle for civil rights.After his return to India in 1915, he set about organising peasants, farmers, and urban labourers to protest against excessive land-tax and discrimination. Assuming leadership of the Indian National Congress in 1921, Gandhi led nationwide campaigns for easing poverty, expanding women's rights, building religious and ethnic amity, ending untouchability, but above all for achieving Swaraj or self-rule Gandhi famously led Indians in challenging the British-imposed salt tax with the 400 km (250 mi) Dandi Salt March in 1930, and later in calling for the British to Quit India in 1942. He was imprisoned for many years, upon many occasions, in both South Africa and India. Gandhi attempted to practise nonviolence and truth in all situations, and advocated that others do the same. He lived modestly in a self-sufficient residential community and wore the traditional Indian dhoti and shawl, woven with yarn hand-spun on a charkha. He ate simple vegetarian food, and also undertook long fasts as a means of both self-purification and social protest.Born and raised in a Hindu merchant caste family in coastal Gujarat, western India, and trained in law at the Inner Temple, London, Gandhi first employed nonviolent civil disobedience as an expatriate lawyer in South Africa, in the resident Indian community's struggle for civil rights.",id("circus"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 2","this is Name 2",id("church"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 3","this is Name 3",id("balloons"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 4","this is Name 4",id("bar"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 5","this is Name 5",id("beach"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 6","this is Name 6",id("carousel"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 7","this is Name 7",id("dream"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 8","this is Name 8",id("eiffel"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 9","this is Name 9",id("muchi"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 10","this is Name 10",id("bumper"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 11","this is Name 11",id("swing"));
        name_list.add(newdata);
        layout_adapter.notifyDataSetChanged();

//recycler view
        RecyclerView.LayoutManager mlayoutmanager=new LinearLayoutManager(Recycler_information.this);
        recyclerView.setLayoutManager(mlayoutmanager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());  //setting animatore
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));  // adding item divider from divideritemdecoration class

        recyclerView.setAdapter(layout_adapter);
        recyclerView.addOnItemTouchListener(
                new Layout_Adapter(getApplicationContext(),recyclerView,new Layout_Adapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {

                        System.out.println("getposition:"+position);

                        View v=recyclerView.getLayoutManager().findViewByPosition(position);
                        TextView textView=(TextView)v.findViewById(R.id.descriptionlayout);
                        ImageView imageView=(ImageView)v.findViewById(R.id.image_recy);
//setting individual image click in recyclerview
                           imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                System.out.println("image:"+position);
                                Toast.makeText(getApplicationContext(),"Image"+position,Toast.LENGTH_SHORT).show();
                                Intent i=new Intent(Recycler_information.this,coordinator_activity.class);
                                Drawable d=name_list.get(position).getimage();
                                Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
                                System.out.println("Recycler activity"+bitmap);
                                i.putExtra("image",bitmap);
                                startActivity(i);
                            }
                        });
//setting textview click in recyclerview
                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                System.out.println("text:"+position);
                                Toast.makeText(getApplicationContext(),"text"+position,Toast.LENGTH_SHORT).show();
                                Intent i=new Intent(Recycler_information.this,Chat.class);
                               // String sdescription=name_list.get(position).getdes();
                               // i.putExtra("description", sdescription);
                                startActivity(i);
                            }
                        });
                    }


                })
        );
      /*  recyclerView.addOnItemTouchListener(
                new Layout_Adapter(getApplicationContext(),recyclerView,new Layout_Adapter.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                       switch(position){

                           case 0:{
                             Intent i=new Intent(Recycler_information.this,Recycler_OnClick.class);
                              String sdescription=name_list.get(position).getdes();
                               i.putExtra("description", sdescription);
                              startActivity(i);
                               System.out.println("first ");
                               //Toast.makeText(getApplicationContext(),"First activity",Toast.LENGTH_SHORT).show();
                               break;
                           }
                          case 1: {
                              Intent i=new Intent(Recycler_information.this,Chat.class);
                             // String sdescription=name_list.get(position).getdes();
                             // i.putExtra("description", sdescription);
                              startActivity(i);

                              Toast.makeText(getApplicationContext(),"second activity",Toast.LENGTH_SHORT).show();
                          break;}
                           case 2: {
                               Intent i=new Intent(Recycler_information.this,coordinator_activity.class);
                               startActivity(i);

                               Toast.makeText(getApplicationContext(), "third activity", Toast.LENGTH_SHORT).show();
                               break;
                           }
                       }
                    }

                    @Override
                    public void onImage(View view, int position) {
                        switch(position) {

                            case 0: {
                                FragmentManager fragmentManager = Recycler_information.this.getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                                onimageclick frag=new onimageclick();
                                fragmentTransaction.add(R.id.frag_frame,frag);
                                fragmentTransaction.commit();
                                break;
                            }
                            case 1: {
                                FragmentManager fragmentManager = Recycler_information.this.getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                                DemoFragment frag=new DemoFragment();
                                fragmentTransaction.add(R.id.frag_frame,frag);
                                fragmentTransaction.commit();

                                break;
                            }
                        }

                    }
                })
        );*/
       // tabLayout.setupWithViewPager(viewPager2);
       // info(name_list);
       // list();

    }

  /*  public void data(){
        Get_Data newdata=new Get_Data("Mahatma Gandhi","Born and raised in a Hindu merchant caste family in coastal Gujarat, western India, and trained in law at the Inner Temple, London, Gandhi first employed nonviolent civil disobedience as an expatriate lawyer in South Africa, in the resident Indian community's struggle for civil rights. After his return to India in 1915, he set about organising peasants, farmers, and urban labourers to protest against excessive land-tax and discrimination. Assuming leadership of the Indian National Congress in 1921, Gandhi led nationwide campaigns for easing poverty, expanding women's rights, building religious and ethnic amity, ending untouchability, but above all for achieving Swaraj or self-rule.\n" +
                "Gandhi famously led Indians in challenging the British-imposed salt tax with the 400 km (250 mi) Dandi Salt March in 1930, and later in calling for the British to Quit India in 1942. He was imprisoned for many years, upon many occasions, in both South Africa and India. Gandhi attempted to practise nonviolence and truth in all situations, and advocated that others do the same. He lived modestly in a self-sufficient residential community and wore the traditional Indian dhoti and shawl, woven with yarn hand-spun on a charkha. He ate simple vegetarian food, and also undertook long fasts as a means of both self-purification and social protest.",id("circus"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 2","this is Name 2",id("church"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 3","this is Name 3",id("balloons"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 4","this is Name 4",id("bar"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 5","this is Name 5",id("beach"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 6","this is Name 6",id("carousel"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 7","this is Name 7",id("dream"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 8","this is Name 8",id("eiffel"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 9","this is Name 9",id("muchi"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 10","this is Name 10",id("bumper"));
        name_list.add(newdata);
        newdata=new Get_Data("Name 11","this is Name 11",id("swing"));
        name_list.add(newdata);
        layout_adapter.notifyDataSetChanged();
        System.out.println(name_list);
    }*/

    //COnverting string into drawable
    private Drawable id(String s){
        Resources res = getResources();
        String mDrawableName = s;
        int resID = res.getIdentifier(mDrawableName , "mipmap", getPackageName());
        Drawable drawable = res.getDrawable(resID );
        return drawable;
    }
//automatic grament change timer
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            if( position >= 4){
                position = 0;
            }else{
                position = position+1;
            }
            viewPager2.setCurrentItem(position, true);
            handler.postDelayed(runnable, 2000);
        }
    };
    public  void onResume(){
        super.onResume();
        viewPager2.postDelayed(runnable,2000);
    }
    public void onPause(){
        super.onPause();
        viewPager2.removeCallbacks(runnable);
    }

    public Activity getActivity() {

        return Recycler_information.this;
    }
}
