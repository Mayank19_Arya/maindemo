package com.simpledemo.mydemo;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class Recycler_OnClick extends AppCompatActivity {
    Recycler_information rc=new Recycler_information();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler__on_click);
       TextView displaytext=(TextView)findViewById(R.id.textView3);
        //getting content form intent on textview click
displaytext.setMovementMethod(new ScrollingMovementMethod());
        Intent intent=getIntent();
        String des=intent.getExtras().getString("description");
        displaytext.setText(des);
    }

}
