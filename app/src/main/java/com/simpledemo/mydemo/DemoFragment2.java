package com.simpledemo.mydemo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class DemoFragment2 extends Fragment {
Button startActivity;
   // display images
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view=inflater.inflate(R.layout.fragment_demo_fragment2, container, false);
        startActivity=(Button)view.findViewById(R.id.startActivity);
        startActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(view.getContext(), MainActivity.class);
                startActivity(i);
            }
        });
        return view;
    }

}
