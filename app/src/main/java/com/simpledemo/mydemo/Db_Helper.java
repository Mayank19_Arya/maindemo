package com.simpledemo.mydemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Db_Helper extends SQLiteOpenHelper{
    public static  final  String database_name="MyDB.db";
    public static final String Login_Table="login";
    public static final String Login_age="age";
    public static final String Login_Address="address";
    public static final String Login_Name="name";
    public static final String Login_TIMESTAMP="timestamp";
    public static final String Login_Id="id";
  //  public static final String Login_Column_Password="password";

    public Db_Helper(Context context) {
        super(context,database_name,null,1);
    }

    //create table
    @Override
    public void onCreate(SQLiteDatabase db) {
      db.execSQL("CREATE TABLE "+Login_Table+"("+Login_Id+"  INTEGER  PRIMARY kEY  AUTOINCREMENT,name text NOT NULL,age integer NOT NULL,address text NOT NULL,timestamp DATETIME DEFAULT CURRENT_DATE)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       // db.execSQL("DROP TABLE IF EXISTS login");

        //onCreate(db);
        db.execSQL("UPDATE login SET timestamp=Strftime('%d-%m-%Y',timestamp)");
        System.out.println("upgrade ran");
    }
//Update login set timestamp=strftime(timestamp/1000000,'unixepoch') where  name='arya'
    public void onDowngrade(SQLiteDatabase db,int oldVersion,int newVersion){
        db.execSQL("UPDATE login SET timestamp=Strftime('%Y-%m-%d',timestamp)");
        System.out.println("Downgrade ran");
    }

    //insert data
    public boolean insert_data(String name,String age,String address){
        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
        //ContentValues contentValues=new ContentValues();
        //contentValues.put("name",name);
       // contentValues.put("id",id);
       // contentValues.put("age",age);
        //contentValues.put("address",address);
        //System.out.println("helper"+"-----"+name + "---" + age + "-----" + address);
       // sqLiteDatabase.insert("login",null,contentValues);
       // database.execSQL("insert into exectable (first_column) values (?);", new String[]{});
       // sqLiteDatabase.rawQuery("INSERT into login (name,age,address) values(?,?,?);",new String[]{name,age,address});
        sqLiteDatabase.execSQL("INSERT into login (name,age,address) values(?,?,?);",new String[]{name,age,address});
       // sqLiteDatabase.execSQL("INSERT into login (name,age,address,timestamp) values(?,?,?,strftime('%d-%m-%Y',CURRENT_TIMESTAMP));",new String[]{name,age,address});
        return true;
    }

    //upating data
    public boolean update_data(String id,String name,String age,String address){
        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
      //  contentValues.put("name",name);
        contentValues.put("age",age);
        contentValues.put("address",address);
        sqLiteDatabase.update("login",contentValues,"name="+name,null);
      //Cursor query=sqLiteDatabase.rawQuery("UPDATE login SET name="+name+"age="+age+"address="+address+"WHERE id="+id+"",null);
        return true;
    }
    //deleting data
    public boolean delete(String id){
        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
      //  sqLiteDatabase.delete("login","name="+id,null);
        //sqLiteDatabase.execSQL("DROP TABLE IF EXISTS login");
       sqLiteDatabase.execSQL("UPDATE login SET timestamp=Strftime('%d-%m-%Y',timestamp)");
        return true;
    }

    public Cursor getData(String id){
        SQLiteDatabase sqLiteDatabase=this.getReadableDatabase();
        Cursor result=sqLiteDatabase.rawQuery("SELECT * FROM login WHERE name=?",new String[]{id});
       // Cursor res=sqLiteDatabase.execSQL("SELECT * FROM login WHERE name=?",new String[]{id});
      //  String data=res.getString(res.getColumnIndex("name"));
        return result;
    }

    public void close(SQLiteDatabase db
            ){
        db.close();
    }
}
