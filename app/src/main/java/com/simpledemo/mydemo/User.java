package com.simpledemo.mydemo;

public class User {
    String name;
    String address;

    public User() {

    }

    public User(String name, String address) {
        this.name = name;
        this.address = address;
    }
    public String getname(){
        return this.name;
    }
    public String getaddress(){
        return this.address;
    }
}
