package com.simpledemo.mydemo;

import android.graphics.drawable.Drawable;

/**
 * Created by Priyanshi on 08-Feb-17.
 */

public class Get_Data {
    public String name,description;
    Drawable imageid;
    public Get_Data(){

    }
    public Get_Data(String name, String description, Drawable id){
        this.name=name;
        this.description=description;
        this.imageid=id;
    }
    public String getname(){
        return name;
    }
    public void setname(String title){
        this.name=title;
    }

    public String getdes(){
        return description;
    }
    public void setdes(String des){
        this.description=des;
    }

    public Drawable getimage(){
        return imageid;
    }
    public void setimage(Drawable  URI){
        this.imageid=URI;
    }
}
