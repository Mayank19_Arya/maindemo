package com.simpledemo.mydemo;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.jar.Attributes;

/**
 * Created by Priyanshi on 08-Feb-17.
 */

public class Layout_Adapter extends RecyclerView.Adapter<Layout_Adapter.MyViewHolder>implements RecyclerView.OnItemTouchListener  {
    public List<Get_Data> namelist;
    private OnItemClickListener mlistener;
    GestureDetector gestureDetector;
    //public static int item;


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
      //  public void onimageclick(View view,int position);
       // public void onImage(View view, int position);
    }


    //onsingle event or on Long press event
   public Layout_Adapter(Context context,final RecyclerView recyclerView, final  OnItemClickListener listener) {
        mlistener = listener;
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent ex) {return true;}
            @Override
            public void onLongPress(MotionEvent e) {
                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (child != null && mlistener != null) {
                    mlistener.onItemClick(child, recyclerView.getChildAdapterPosition(child));
                }
            }
        });


    }
 /*   public Layout_Adapter(Context context, final RecyclerView recyclerView, OnItemClickListener listener){
       mlistener=listener;
        final  int i=getdata();
        gestureDetector=new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
           /* public boolean onSingleTapUp(MotionEvent e){
               System.out.println("tried onSingleTapUp");
                return true;
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
            public boolean onDoubleTap(MotionEvent e) {
                View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
                System.out.println("tried onSingleTapUp");
                if (childView != null && mlistener != null && gestureDetector.onTouchEvent(e)) {
                    mlistener.onimageclick(childView, i);
                }
                return true;
            }
        });
    }
*/

    //on touch event
    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View childView = rv.findChildViewUnder(e.getX(), e.getY());
        if(childView != null && mlistener != null && gestureDetector.onTouchEvent(e))
        {
            mlistener.onItemClick(childView,rv.getChildAdapterPosition(childView));
        }
       return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

//inflating view
    @Override
    public Layout_Adapter.MyViewHolder onCreateViewHolder (ViewGroup parent,int viewType){
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_view, parent, false);
        return new MyViewHolder(itemview);
    }


    public Layout_Adapter(List < Get_Data > name_list) {

        this.namelist = name_list;
    }

    //setting views
    @Override
    public void onBindViewHolder (Layout_Adapter.MyViewHolder holder, final int position){
        Get_Data name_list = namelist.get(position);
        holder.name.setText(name_list.getname());
        //setOnClickListener(holder.name,position);
        holder.description.setText(name_list.getdes());
       // setOnClickListener(holder.description,position);
        holder.imageView.setImageDrawable(name_list.getimage());
        //setOnClickListener(holder.imageView,position);
    }

  /*  private int setOnClickListener(View view, final int position) {
      view.setOnClickListener(new View.OnClickListener() {

          @Override
          public void onClick(View v) {
              if (v.getId() == R.id.namelayout) {
                  System.out.println("Name");
              }
              else if (v.getId() == R.id.descriptionlayout) {
                  item=0;
                  System.out.println("item set in onclick for description"+item+"position"+position);
              } else if (v.getId() == R.id.image_recy) {
                  item=1;
                  System.out.println("item set in onclick for image "+item+"position:"+position);
              }
          }
      });

        return item;
    }

*/
    @Override
    public int getItemCount () {
        return namelist.size();
    }


    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
//initialsing views
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, description;
        public ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.namelayout);
            description = (TextView) itemView.findViewById(R.id.descriptionlayout);
            imageView = (ImageView) itemView.findViewById(R.id.image_recy);
        }
    }
}
