package com.simpledemo.mydemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Chat extends AppCompatActivity {
private ListView listView;
    private List<ChatMessage> list;
    private List<String> data_list=new ArrayList<>();
    private boolean isMine=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        list=new ArrayList<>();
        listView=(ListView)findViewById(R.id.listview);  //initialising list
        String s=new String("Helllo, its me");             //adding data to list
        data_list.add(s);
        s=new String("I was wondering if after all these years you'ld like to meet,");
        data_list.add(s);
        s=new String("They say that time's supposed to heal ya");
        data_list.add(s);
        s=new String("But i ain't done much healing,");
        data_list.add(s);
        s=new String("I am in california dreaming about who we used to be");
        data_list.add(s);
        s=new String("When we were younger and free");
        data_list.add(s);
        s=new String("I've forgotten how it felt before the world fell at our feet");
        data_list.add(s);
        s=new String("There's such a difference between us");
        data_list.add(s);
        s=new String("And a million miles");
        data_list.add(s);
        s=new String("Hello from the other side");
        data_list.add(s);
        s=new String("I must have called a thousand times");
        data_list.add(s);
        s=new String("To tell you I'm sorry for everything that I've done");
        data_list.add(s);
        s=new String("But when I call you never seem to be home");
        data_list.add(s);
        s=new String("Hello from the outside");
        data_list.add(s);
        s=new String("At least I can say that I've tried");
        data_list.add(s);
        s=new String("To tell you I'm sorry for breaking your heart");
        data_list.add(s);
        s=new String("But it don't matter it clearly doesn't tear you apart anymore");
        data_list.add(s);
        s=new String("Say Hello form the outside");
        data_list.add(s);
        s=new String("There's such a difference between us");
        data_list.add(s);
        s=new String("And a million miles");
        data_list.add(s);
        s=new String("Hello from the other side");
        data_list.add(s);
        s=new String("I must have called a thousand times");
        data_list.add(s);
        s=new String("To tell you I'm sorry for everything that I've done");
        data_list.add(s);
        s=new String("But when I call you never seem to be home");
        data_list.add(s);
        for(int i=0;i<18;i++)             //setting up booleantrue false for data in the list
        {
           ChatMessage m=new ChatMessage(value(i),true);
            list.add(m);
            i++;
            m=new ChatMessage(value(i),false);
            list.add(m);
            i++;
            m=new ChatMessage(value(i),false);
            list.add(m);

        }
          /* ChatMessage msg;
     ChatMessage msg1=new ChatMessage("Helllo, its me,",true);
        list.add(msg1);
        msg=new ChatMessage("I was wondering if after all these years you'ld like to meet,",false);
        list.add(msg);
        msg=new ChatMessage("to go over everything. ",true);
        list.add(msg);
        msg=new ChatMessage("They say that time's supposed to heal ya",false);
        list.add(msg);
        msg=new ChatMessage("But i ain't done much healing,",true);
        list.add(msg);
        msg=new ChatMessage("I am in california dreaming about who we used to be",false);
        list.add(msg);
        msg=new ChatMessage("When we were younger and free",true);
        list.add(msg);
        msg=new ChatMessage("I've forgotten how it felt before the world fell at our feet",false);
        list.add(msg);
        msg=new ChatMessage("There's such a difference between us",true);
        list.add(msg);
        msg=new ChatMessage("And a million miles",false);
        list.add(msg);
        msg=new ChatMessage("Hello from the other side",true);
        list.add(msg);
        msg=new ChatMessage("I must have called a thousand times",false);
        list.add(msg);
        msg=new ChatMessage("To tell you I'm sorry for everything that I've done",true);
        list.add(msg);
        msg=new ChatMessage("But when I call you never seem to be home",false);
        list.add(msg);
        msg=new ChatMessage("Hello from the outside",true);
        list.add(msg);
        msg=new ChatMessage("At least I can say that I've tried",false);
        list.add(msg);
        msg=new ChatMessage("To tell you I'm sorry for breaking your heart",true);
        list.add(msg);
        msg=new ChatMessage("But it don't matter it clearly doesn't tear you apart anymore",false);
        list.add(msg);
        */
        ArrayAdapter<ChatMessage> adapter = new MessageAdapter(this, R.layout.activity_chat_left, list);
        adapter.notifyDataSetChanged();                       //data set changed update
        listView.setAdapter(adapter);
    }

    //getting value at i posiiton in the list
    private String value(int i) {
       String str;
        str=this.data_list.get(i);
        return str;
    }

}
