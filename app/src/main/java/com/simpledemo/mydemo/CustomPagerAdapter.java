package com.simpledemo.mydemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.TableLayout;

import static java.security.AccessController.getContext;

public class CustomPagerAdapter extends FragmentPagerAdapter {
    protected Context mcontext;

    public CustomPagerAdapter(FragmentManager fm,Context context) {
        super(fm);
        mcontext=context;
    }
//returning fragment to viewpager
    @Override
    public Fragment getItem(int position) {
        Fragment fragment=new DemoFragment();
        Bundle args=new Bundle();
        args.putInt("position:",position+1);
        fragment.setArguments(args);

        switch (position){
            case 1: return new DemoFragment3();
            case 2: return new DemoFragment2();
            default: return fragment;
        }


    }

    @Override
    public int getCount() {
        return 3;
    }

}
