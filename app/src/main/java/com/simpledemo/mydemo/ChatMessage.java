package com.simpledemo.mydemo;

/**
 * Created by Priyanshi on 10-Feb-17.
 */

public class ChatMessage {
    private String content;
    public boolean isMine;

    public ChatMessage(String Content,boolean isMine){
        this.content=Content;
        this.isMine=isMine;
    }

    public boolean getMine(){
        return this.isMine;
    }

    public String getContent() {
        return this.content;
    }

}
