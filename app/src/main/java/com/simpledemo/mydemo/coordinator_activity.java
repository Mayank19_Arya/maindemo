package com.simpledemo.mydemo;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import java.text.NumberFormat;

import static android.R.attr.data;

public class coordinator_activity extends AppCompatActivity {
private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordinator_activity);
        Toolbar snackbar = (Toolbar) findViewById(R.id.toolbar);
        //setting up homebar and back button
        setSupportActionBar(snackbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //getting extra intent to be used to set image from recyclerviewactivity
        imageView = (ImageView) findViewById(R.id.toolbarImage);
        Intent intent = getIntent();
        Bitmap bitmap = (Bitmap)intent.getParcelableExtra("image");
        imageView.setImageBitmap(bitmap);

      // imageView.setImageDrawable(drawable);

        }

    //back button method
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
}
        }
