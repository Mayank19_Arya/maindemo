package com.simpledemo.mydemo;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Priyanshi on 07-Feb-17.
 */

public class Custom_Main extends FragmentPagerAdapter {
    protected Context mcontext;

    public Custom_Main(FragmentManager fragmentManager, Context context) {
        super(fragmentManager);
        mcontext=context;
    }

    //getItem fragment
    @Override
    public Fragment getItem(int position) {
        Fragment fragment=new Main_Frag();
        Bundle args=new Bundle();
        args.putInt("position:",position+1);
        fragment.setArguments(args);

        switch (position){
            case 1: return new Main_Fragment3();
            case 2: return new MainFragment2();
            default: return fragment;
        }


    }

    //get count
    @Override
    public int getCount() {
        return 3;
    }

}
