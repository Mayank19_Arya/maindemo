package com.simpledemo.mydemo;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Custom_Main customPagerAdapter;
    ViewPager viewPager1;
    int position;
    public static final int delay=500;
    TextView clickabletext,squaretext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //clickable text circle
         clickabletext=(TextView)findViewById(R.id.textView);
        clickabletext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Circle clicked",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(MainActivity.this,cardview_activity.class);
                startActivity(intent);
            }
        });
        //clickable text square
        squaretext=(TextView)findViewById(R.id.textView2);
        squaretext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent=new Intent(MainActivity.this,Recycler_information.class);
                startActivity(intent);

            }
        });
        //view pager nand 3 dots in Tab layout
        TabLayout tabLayout = (TabLayout)findViewById(R.id.tab_layout1);
        customPagerAdapter =new Custom_Main(getSupportFragmentManager(),MainActivity.this);
        viewPager1=(ViewPager)findViewById(R.id.scrollview1);
        viewPager1.setAdapter(customPagerAdapter);

        tabLayout.setupWithViewPager(viewPager1);

    }

    //automatic fragment rotation
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            if( position >= 4){
                position = 0;
            }else{
                position = position+1;
            }
            viewPager1.setCurrentItem(position, true);
            handler.postDelayed(runnable, 2000);
        }
    };
    public  void onResume(){
        super.onResume();
        viewPager1.postDelayed(runnable,2000);
    }
    public void onPause(){
        super.onPause();
        viewPager1.removeCallbacks(runnable);
    }
}
