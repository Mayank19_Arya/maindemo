package com.simpledemo.mydemo;

/**
 * Created by Priyanshi on 22-Feb-17.
 */

public class card {
    private String name;
    private String email;
    public card(String name,String email){
        this.name=name;
        this.email=email;
    }
    public String getname(){
        return this.name;
    }
    public String getemail(){
        return this.email;
    }
    public void setName(String name){
        this.name=name;
    }
    public void setEmail(String email){
        this.email=email;
    }
}
