package com.simpledemo.mydemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class cardview_activity extends AppCompatActivity {
public ListView cardlistview;
    public List<card> card_list=new ArrayList<>();
    public cardview_adapter cardview_adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardview_activity);

        final RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recycle_cardview); //initialsing recyclerview
        cardview_adapter=new cardview_adapter(card_list);                    //initialsing cardview adapter
        card data=new card("mayank","mayank.arya@virtuos.com");          //adding data to list
        card_list.add(data);
        data=new card("xyz","xyz@gmail.com");
        card_list.add(data);
        data=new card("xyz1","xyz1@gmail.com");
        card_list.add(data);
        data=new card("xyz2","xyz2@gmail.com");
        card_list.add(data);
        data=new card("xyz3","xyz3@gmail.com");
        card_list.add(data);
        data=new card("xyz4","xyz4@gmail.com");
        card_list.add(data);
        cardview_adapter.notifyDataSetChanged();                         //notifying changes
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(this);     //seeting up layout manager
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(cardview_adapter);
    }
}
